import re
import csv
import xml.etree.ElementTree as ET
from pathlib import Path

# Common variables for all cards
root = (Path(__file__).parent / "..").resolve()
csv = csv.reader((root / "info.csv").open(), delimiter=';', quotechar='"')

# Modifies layers
def mod_layers(tree, tag, i, col):
    for g in tree.iter(tag):
        if g.attrib["id"] == "cut":
            tree.remove(g)
        if g.attrib["id"] == "certified" and i == 10 and col == "":
            tree.remove(g)

# Modifies background
def mod_background(tree, tag, i, col):
    for path in tree.iter(tag):
        if "field" in path.attrib and i == 11:
            style = re.sub(r'fill:#\w+', r'fill:#' + col, path.attrib["style"])
            path.attrib["style"] = style

# Modifies text fields
def mod_text(tree, tag, i, col):
    for el in tree.iter(tag):
        if "field" in el.attrib and el.attrib["field"] == str(i):
            el.text = f"{col} "

# Modifies template
def mod_template(template, i, col):
    pre = "{http://www.w3.org/2000/svg}"
    mod_layers(template.getroot(), f"{pre}g", i, col)
    mod_background(template.getroot(), f"{pre}path", i, col)
    mod_text(template.getroot(), f"{pre}tspan", i, col)

# Each row is a person and each col is a field
for row in list(csv)[1:]:
    label = row[0].split(" ")[0].lower()
    template_a = ET.parse((root / "template_a.svg").open())
    template_b = ET.parse((root / "template_b.svg").open())
    for i, col in enumerate(row):
        for template in [template_a, template_b]:
            suf = "a" if template == template_a else "b"
            mod_template(template, i, col)
            template.write(root / "paths" / f"{label}_{suf}.svg")
